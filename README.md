# openldap-backup.sh
A simple backup script for OpenLDAP, forked from [https://gitlab.com/unxis/mariadb-backup.sh](mariadb-backup.sh.)

# Introduction

This script is a fork of simple cron based backup for MariaDB.  The common backup process and history is used here, however the guts have obviously been replaced by OpenLDAP specific steps.

### Caterpillar icon license

This project uses a modified version of the [Caterpillar PNG Icon](https://www.pngrepo.com/svg/192630/industrial-robot-factory) 
under a [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
license.
